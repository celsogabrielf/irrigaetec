import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'prjIrrigaEtec';

  @ViewChild('header') private headerRef: ElementRef;

  private get header(): HTMLCanvasElement {
    return this.headerRef.nativeElement;
  }

  @ViewChild('menuToggle') private menuToggleRef: ElementRef;

  private get menuToggle(): HTMLCanvasElement {
    return this.menuToggleRef.nativeElement;
  }

  @ViewChild('navigation') private navigationRef: ElementRef;

  private get navigation(): HTMLCanvasElement {
    return this.navigationRef.nativeElement;
  }


  @HostListener('window: scroll', ['$event'])
  onScroll(_event: Event) {
    this.header.classList.toggle("stricky", window.scrollY > 0);
  };

  toggleMenu(){
    this.menuToggle.classList.toggle('active');
    this.navigation.classList.toggle('active');
  }

  constructor() {
  }

  ngOnInit(): void {

  }
}
